import { Reducer } from 'redux'

import { Status } from './Status'
import { generateState, State } from './state'
import {
  ActionTypes,
  SUCCESS_ACTION,
  FAILURE_ACTION,
  generateActionsType,
  ACTION_WITH_PAYLOAD
} from './actions'

export function generateReducer<StoredDataType, ErrorDataType>(
  baseAction: string,
  initialData: StoredDataType | undefined,
  initialError: ErrorDataType
) {
  const initialState = generateState<StoredDataType | undefined, ErrorDataType>(
    initialData === undefined ? ({} as StoredDataType) : initialData,
    initialError
  )
  const { START, SUCCESS, FAILURE, RESET } = generateActionsType(baseAction)

  return function(
    state: State<StoredDataType, ErrorDataType> = initialState,
    action: ActionTypes<StoredDataType, ErrorDataType> | { type: '@@INIT' }
  ): State<StoredDataType, ErrorDataType> {
    switch (action.type) {
      case START:
        return {
          ...state,
          status: Status.LOADING
        }
      case SUCCESS:
        return {
          ...state,
          status: Status.SUCCESS,
          ...(initialData === undefined ? {} : (action as SUCCESS_ACTION<StoredDataType>).payload)
        }
      case FAILURE:
        return {
          ...state,
          status: Status.FAILURE,
          error: (action as FAILURE_ACTION<ErrorDataType>).payload.error
        }
      case RESET:
        return initialState
      default:
        return state
    }
  }
}

export function generateSimpleReducer<StateType>(
  baseAction: string,
  initialState: StateType
): [
  Reducer<StateType, ACTION_WITH_PAYLOAD<StateType> | { type: '@@INIT' }>,
  (payload: StateType) => ACTION_WITH_PAYLOAD<StateType>
] {
  const reducer = function(
    state: StateType = initialState,
    action: ACTION_WITH_PAYLOAD<StateType> | { type: '@@INIT' }
  ): StateType {
    switch (action.type) {
      case baseAction:
        return {
          ...state,
          ...(action as ACTION_WITH_PAYLOAD<StateType>).payload
        }
      default:
        return state
    }
  }

  const action = (payload: StateType) => ({
    type: baseAction,
    payload
  })

  return [reducer, action]
}
