/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  generateActionsType,
  generateActions,
  generateActionType,
  generateStartAction,
  generateSuccessAction,
  generateFailureAction,
  generateResetAction
} from './actions'
import { generateReducer, generateSimpleReducer } from './reducer'
import generateSaga, { WorkersConfiguration } from './saga'
import { Status } from './Status'

/**
 * Generate and return a reducer, a saga worker and a list of actions with the given properties.
 *
 *
 * @param baseAction - String used to build redux action. "TODO" will create "TODO_START", "TODO_SUCCESS", "TODO_FAILURE" and "TODO_RESET" actions
 * @param initialData - Initial value of the property data in the created reducer
 * @param initialError - Initial value of the error in the created reducer
 * @param workers - Initial value of the property data in the created reducer
 * @returns A reducer, its saga and actions
 *
 * @beta
 */
export default function generateRedux<
  StartPayloadType,
  SuccessPayloadType,
  ErrorDataType,
  StoredDataType = SuccessPayloadType
>(
  baseAction: string,
  initialData: StoredDataType | undefined,
  initialError: ErrorDataType,
  workersConfig: WorkersConfiguration<StartPayloadType, SuccessPayloadType, ErrorDataType>
) {
  const reducer = generateReducer<StoredDataType, ErrorDataType>(
    baseAction,
    initialData,
    initialError
  )

  const { saga, workers } = generateSaga<StartPayloadType, SuccessPayloadType, ErrorDataType>(
    baseAction,
    workersConfig
  )

  const actions = generateActions<StartPayloadType, SuccessPayloadType, ErrorDataType>(baseAction)

  return { reducer, saga, workers, actions }
}

export {
  generateActionsType,
  generateActionType,
  generateActions,
  generateStartAction,
  generateSuccessAction,
  generateFailureAction,
  generateResetAction,
  generateReducer,
  generateSimpleReducer,
  generateSaga,
  Status
}
