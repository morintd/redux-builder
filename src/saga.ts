/* eslint-disable @typescript-eslint/no-explicit-any */
import { takeLatest, put, call } from 'redux-saga/effects'

import { START_ACTION, generateActionsType, SUCCESS_ACTION, FAILURE_ACTION } from './actions'

export type WorkersConfiguration<StartPayloadType, SuccessPayloadType, ErrorDataType> = {
  onStart?: (payload: StartPayloadType) => Promise<SuccessPayloadType> | Generator
  onSuccess?: (payload: SuccessPayloadType) => void
  onFailure?: (payload: ErrorDataType) => void
  onReset?: () => void
}

type Workers = {
  onStart?: any
  onSuccess?: any
  onFailure?: any
  onReset?: any
}

function generateSaga<StartPayloadType, SuccessPayloadType, ErrorDataType>(
  baseAction: string,
  workersConfig: WorkersConfiguration<StartPayloadType, SuccessPayloadType, ErrorDataType>
) {
  const { START, SUCCESS, FAILURE, RESET } = generateActionsType(baseAction)
  const { onStart, onSuccess, onFailure, onReset } = workersConfig

  const workers: Workers = {}

  if (onStart) {
    workers.onStart = function*(action: START_ACTION<StartPayloadType>) {
      try {
        const data = yield call(onStart, action.payload)
        yield put({ type: SUCCESS, payload: data })
      } catch (e) {
        yield put({ type: FAILURE, payload: { error: e.message } })
      }
    }
  }

  if (onSuccess) {
    workers.onSuccess = function*(action: SUCCESS_ACTION<SuccessPayloadType>) {
      yield call(onSuccess, action.payload)
    }
  }

  if (onFailure) {
    workers.onFailure = function*(action: FAILURE_ACTION<ErrorDataType>) {
      yield call(onFailure, action.payload.error)
    }
  }

  if (onReset) {
    workers.onReset = function*() {
      yield call(onReset)
    }
  }

  const saga = function*() {
    if (onStart) yield takeLatest(START, workers.onStart)
    if (onSuccess) yield takeLatest(SUCCESS, workers.onSuccess)
    if (onFailure) yield takeLatest(SUCCESS, workers.onFailure)
    if (onReset) yield takeLatest(RESET, workers.onReset)
  }

  return { saga, workers }
}

export default generateSaga
