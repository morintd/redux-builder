import { Status } from './Status'

export type ACTION_WITH_PAYLOAD<T> = {
  type: string
  payload: T
}

export type START_ACTION<PayloadType = void> = ACTION_WITH_PAYLOAD<PayloadType>

export type SUCCESS_ACTION<PayloadType> = ACTION_WITH_PAYLOAD<PayloadType>

export type FAILURE_ACTION<ErrorType> = {
  type: string
  payload: {
    error: ErrorType
  }
}

export type RESET_ACTION = {
  type: string
}

export function generateActionType(baseAction: string, status?: Status) {
  return `${baseAction}_${status ? status.split('_')[1] : 'RESET'}`
}

export function generateActionsType(baseAction: string) {
  const START = generateActionType(baseAction, Status.START)
  const SUCCESS = generateActionType(baseAction, Status.SUCCESS)
  const FAILURE = generateActionType(baseAction, Status.FAILURE)
  const RESET = generateActionType(baseAction)

  return { START, SUCCESS, FAILURE, RESET }
}

export const generateStartAction = <StartPayloadType>(baseAction: string) => (
  payload: StartPayloadType
) =>
  ({
    type: generateActionType(baseAction, Status.START),
    payload
  } as START_ACTION<StartPayloadType>)

export const generateSuccessAction = <SuccessPayloadType>(baseAction: string) => (
  payload: SuccessPayloadType
) =>
  ({
    type: generateActionType(baseAction, Status.SUCCESS),
    payload
  } as SUCCESS_ACTION<SuccessPayloadType>)

export const generateFailureAction = <ErrorDataType>(baseAction: string) => (
  error: ErrorDataType
) =>
  ({
    type: generateActionType(baseAction, Status.FAILURE),
    payload: {
      error
    }
  } as FAILURE_ACTION<ErrorDataType>)

export const generateResetAction = (baseAction: string) => () =>
  ({
    type: generateActionType(baseAction)
  } as RESET_ACTION)

export function generateActions<StartPayloadType, SuccessPayloadType, ErrorDataType>(
  baseAction: string
): [
  (payload: StartPayloadType) => START_ACTION<StartPayloadType>,
  (data: SuccessPayloadType) => SUCCESS_ACTION<SuccessPayloadType>,
  (error: ErrorDataType) => FAILURE_ACTION<ErrorDataType>,
  () => RESET_ACTION
] {
  return [
    generateStartAction<StartPayloadType>(baseAction),
    generateSuccessAction<SuccessPayloadType>(baseAction),
    generateFailureAction<ErrorDataType>(baseAction),
    generateResetAction(baseAction)
  ]
}

export type ActionTypes<SuccessDataType, ErrorDataType> =
  | START_ACTION
  | SUCCESS_ACTION<SuccessDataType>
  | FAILURE_ACTION<ErrorDataType>
  | RESET_ACTION
