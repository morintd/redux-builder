import { Status } from './Status'

export type State<StoredDataType, ErrorDataType> = {
  status: Status
  error: ErrorDataType
} & StoredDataType

export const generateState = <StoredDataType, ErrorDataType>(
  data: StoredDataType,
  error: ErrorDataType
): State<StoredDataType, ErrorDataType> => ({
  status: Status.START,
  error,
  ...data
})
