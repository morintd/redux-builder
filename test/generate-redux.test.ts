import generateRedux, { Status } from '../src/redux-builder'
import { Action } from 'redux'
import { runSaga, Saga } from 'redux-saga'
import { call } from 'redux-saga/effects'

async function recordSaga(saga: Saga<any>, initialAction: Action) {
  const dispatched: Array<Function> = []
  await runSaga(
    {
      dispatch: (action: Function) => dispatched.push(action)
    },
    saga,
    initialAction
  ).toPromise()
  return dispatched
}

describe('redux-builder', () => {
  describe('generateRedux', () => {
    type StartDataType = { slug: string }
    type ArticleType = { title: string; slug: string; content: string }
    const nullArticle = { title: '', slug: '', content: '' }
    type SuccessDataType = { articles: Array<ArticleType> }

    const { reducer, workers, actions } = generateRedux<StartDataType, SuccessDataType, string>(
      'FETCH_ARTICLE',
      { articles: [] },
      '',
      {
        onStart: function*(payload: StartDataType) {
          const request = (slug: string) =>
            Promise.resolve(
              [{ title: 'title', slug: 'slug', content: 'content' }].filter(
                article => article.slug === slug
              )
            )

          const articles = yield call(request, payload.slug)
          return { articles }
        }
      }
    )

    const [fetchArticles, fetchArticlesSuccess, fetchArticlesFailure, fetchArticlesReset] = actions

    describe('reducer', () => {
      const initialState = {
        status: Status.START,
        error: '',
        articles: []
      }

      test('should return the initial state', () => {
        const state = reducer(undefined, { type: '@@INIT' })
        expect(state).toEqual(initialState)
      })

      test('should handle fetchArticles', () => {
        const state = reducer(undefined, fetchArticles({ slug: 'slug' }))
        expect(state).toEqual({ articles: [], error: '', status: Status.LOADING })
      })

      test('should handle fetchArticlesSuccess', () => {
        const state = reducer(undefined, fetchArticlesSuccess({ articles: [nullArticle] }))
        expect(state).toEqual({ articles: [nullArticle], error: '', status: Status.SUCCESS })
      })

      test('should handle fetchArticlesFailure', () => {
        const state = reducer(undefined, fetchArticlesFailure('test-failure'))
        expect(state).toEqual({ status: Status.FAILURE, error: 'test-failure', articles: [] })
      })

      test('should handle fetchArticlesReset', () => {
        const successState = reducer(undefined, fetchArticlesSuccess({ articles: [nullArticle] }))
        const state = reducer(successState, fetchArticlesReset())
        expect(state).toEqual(initialState)
      })
    })
    describe('saga', () => {
      describe('onStart', () => {
        test('should dispatch a success event with given data', async () => {
          const dispatched = await recordSaga(workers.onStart, fetchArticles({ slug: 'slug' }))
          expect(dispatched).toEqual([
            fetchArticlesSuccess({
              articles: [{ title: 'title', content: 'content', slug: 'slug' }]
            })
          ])
        })
      })
    })
  })
})
