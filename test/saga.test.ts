import {
  generateSuccessAction,
  generateSaga,
  generateStartAction,
  generateFailureAction,
  generateResetAction
} from '../src/redux-builder'
import { Saga, runSaga } from 'redux-saga'
import { Action } from 'redux'
import { call, put } from 'redux-saga/effects'

type ArticleType = {
  title: string
}

async function recordSaga(saga: Saga<any>, initialAction: Action) {
  const dispatched: Array<Function> = []
  await runSaga(
    {
      dispatch: (action: Function) => dispatched.push(action)
    },
    saga,
    initialAction
  ).toPromise()
  return dispatched
}

describe('saga', () => {
  describe('generateSaga', () => {
    const baseAction = 'FETCH_ARTICLES'
    const fetchArticles = generateStartAction<{ page: number }>(baseAction)
    const fetchArticlesSuccess = generateSuccessAction<{ articles: Array<ArticleType> }>(baseAction)
    const fetchArticlesFailure = generateFailureAction<string>(baseAction)
    const fetchArticlesReset = generateResetAction(baseAction)
    const { workers } = generateSaga<{ page: number }, { articles: Array<ArticleType> }, string>(
      baseAction,
      {
        onStart: function*(payload) {
          const request = (page: number) =>
            new Promise((resolve, reject) => {
              if (page === 1) {
                resolve([{ title: 'title' }])
              } else reject(new Error('mock-error'))
            })

          const articles = yield call(request, payload.page)
          return { articles }
        },
        onSuccess: function*(payload) {
          yield put({ type: 'another-action-success', payload })
        },
        onFailure: function*() {
          yield put({ type: 'another-action-failure' })
        },
        onReset: function*() {
          yield put({ type: 'another-action-reset' })
        }
      }
    )
    describe('onStart', () => {
      test('should dispatch a success action if given function resolve', async () => {
        const dispatched = await recordSaga(workers.onStart, fetchArticles({ page: 1 }))
        expect(dispatched).toEqual([fetchArticlesSuccess({ articles: [{ title: 'title' }] })])
      })
      test('should dispatch a failure action if given function reject', async () => {
        const dispatched = await recordSaga(workers.onStart, fetchArticles({ page: 2 }))
        expect(dispatched).toEqual([fetchArticlesFailure('mock-error')])
      })
    })
    describe('onSuccess', () => {
      test('should execute given instructions', async () => {
        const dispatched = await recordSaga(
          workers.onSuccess,
          fetchArticlesSuccess({ articles: [{ title: 'title' }] })
        )
        expect(dispatched).toEqual([
          { type: 'another-action-success', payload: { articles: [{ title: 'title' }] } }
        ])
      })
    })
    describe('onFailure', () => {
      test('should execute given instructions', async () => {
        const dispatched = await recordSaga(
          workers.onFailure,
          fetchArticlesSuccess({ articles: [{ title: 'title' }] })
        )
        expect(dispatched).toEqual([{ type: 'another-action-failure' }])
      })
    })
    describe('onReset', () => {
      test('should execute given instructions', async () => {
        const dispatched = await recordSaga(workers.onReset, fetchArticlesReset())
        expect(dispatched).toEqual([{ type: 'another-action-reset' }])
      })
    })
  })
})
