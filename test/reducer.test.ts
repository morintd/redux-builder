import {
  Status,
  generateReducer,
  generateSuccessAction,
  generateSimpleReducer
} from '../src/redux-builder'

describe('reducer', () => {
  describe('generateReducer', () => {
    describe('With no stored data', () => {
      const baseAction = 'FETCH_ARTICLES'
      const reducer = generateReducer<void, string>(baseAction, undefined, '')
      const fetchArticlesSuccess = generateSuccessAction<void>(baseAction)

      test('should only store error and status', () => {
        const state = reducer(undefined, { type: '@@INIT' })
        expect(state).toEqual({ status: Status.START, error: '' })
      })

      test('should only set status as Success on success action', () => {
        const state = reducer(undefined, fetchArticlesSuccess())
        expect(state).toEqual({ status: Status.SUCCESS, error: '' })
      })
    })
  })

  describe('generateSimpleReducer', () => {
    const baseAction = 'SET_LANGUAGE'
    const [reducer, setLanguage] = generateSimpleReducer<{ language: string }>(baseAction, {
      language: 'en'
    })
    test('should not store status or error', () => {
      const state = reducer(undefined, { type: '@@INIT' })
      expect(state).toEqual({ language: 'en' })
    })
    test('should handle generated action', () => {
      const state = reducer(undefined, setLanguage({ language: 'fr' }))
      expect(state).toEqual({ language: 'fr' })
    })
  })
})
